type FontsLinksObject = Record<string, string>;

const customFontsLinks: FontsLinksObject = {
  inter: 'Assets/Inter-Regular.ttf#Inter',
  interBlack: 'Assets/Inter-Black.ttf#Inter Black',
  interMedium: 'Assets/Inter-Medium.ttf#Inter Medium',
  interBold: 'Assets/Inter-Bold.ttf#Inter Bold',
  interSemibold: 'Assets/Inter-SemiBold.ttf#Inter SemiBold',
  interExtraBold: 'Assets/Inter-ExtraBold.ttf#Inter ExtraBold',
  interThin: 'Assets/Inter-Thin.ttf#Inter Thin',
  interLight: 'Assets/Inter-Light.ttf#Inter Light',
  interExtraLight: 'Assets/Inter-ExtraLight.ttf#Inter ExtraLight',
};

export default customFontsLinks;
