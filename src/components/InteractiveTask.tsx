import React, {useState} from 'react';
import {View, StyleSheet, Text, StyleProp, ViewStyle} from 'react-native';
import {useTheme, Button} from 'react-native-paper';
// @ts-ignore
import Icon from 'react-native-vector-icons/dist/Ionicons';
import SelectHeaderTask from './SelectHeaderTask.tsx';

export type interactiveTaskAnswer = {id: string; isTrue: boolean};

interface InteractiveTaskProps {
  number: number;
  title: string;
  description: string;
  style?: StyleProp<ViewStyle>;
}

const InteractiveTask: React.FC<InteractiveTaskProps> = ({
  number,
  title,
  description,
  style,
}) => {
  const {colors, roundness} = useTheme();

  const [isBlocked, setIsBlocked] = useState<boolean>(false);
  const [userAnswer, setUserAnswer] = useState<
    interactiveTaskAnswer | undefined
  >();

  const [iconName, setIconName] = useState<string>('');

  const answerCorrect = (): boolean | undefined => userAnswer?.isTrue;

  const checkUserAnswer = (): void => {
    if (answerCorrect()) {
      setIconName('checkmark-outline');
    } else {
      setIconName('close-outline');
    }
  };

  const complete = (): void => {
    setIsBlocked(true);
    checkUserAnswer();
  };

  return (
    <View
      style={[
        styles.container,
        {backgroundColor: colors.primaryContainer, borderRadius: roundness},
        style,
      ]}>
      <View style={[styles.titleContainer]}>
        <Text style={[styles.title, {color: colors.onPrimaryContainer}]}>
          Задание №{number}: {title}
        </Text>
        <Icon
          size={24}
          name={iconName}
          color={iconName === 'checkmark-outline' ? 'green' : 'red'}
        />
      </View>
      <Text style={[styles.description, {color: colors.onPrimaryContainer}]}>
        {description}
      </Text>
      <View style={styles.userActionsBlock}>
        <SelectHeaderTask setAnswer={setUserAnswer} isBlocked={isBlocked} />
        <Button
          style={[styles.button, {borderRadius: roundness}]}
          contentStyle={{height: '100%'}}
          buttonColor={colors.primary}
          textColor={colors.onPrimary}
          onPress={complete}>
          Завершить
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    padding: 15,
  },
  titleContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    gap: 20,
  },
  title: {
    fontFamily: 'Inter',
    fontSize: 16,
    fontWeight: '500',
  },
  description: {
    fontFamily: 'Inter',
    marginTop: 8,
    fontSize: 12,
    fontWeight: '400',
  },
  userActionsBlock: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 8,
    alignItems: 'flex-start',
  },
  button: {
    width: '20%',
    marginLeft: 10,
    height: 46,
  },
});

export default InteractiveTask;
