import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  NativeSyntheticEvent,
  TextInputChangeEventData,
} from 'react-native-windows';
import {Button} from 'react-native-paper';
// @ts-ignore
import Icon from 'react-native-vector-icons/dist/Ionicons';
import BaseInput from './ui/BaseInput';
import {useTheme} from 'react-native-paper';

interface TaskWithInputProps {
  number: number;
  title: string;
  description: string;
  solution: string;
  style: object | undefined;
}

const TaskWithInput: React.FC<TaskWithInputProps> = ({
  number,
  title,
  description,
  solution,
  style,
}) => {
  const {colors, roundness} = useTheme();

  const [isBlocked, setIsBlocked] = React.useState<boolean>(false);
  const [answer, setAnswer] = React.useState<string>('');

  const [iconName, setIconName] = React.useState<string>('');

  const handleInputChange = (
    event: NativeSyntheticEvent<TextInputChangeEventData>,
  ): void => {
    setAnswer(event.nativeEvent.text);
  };
  const handleButtonPress = (): void => {
    if (answer.length > 0) {
      checkAnswer();
    }
  };

  const checkAnswer = (): void => {
    setIsBlocked(true);
    const isTrue = answer.toLowerCase() === solution.toLocaleLowerCase();

    if (isTrue) {
      setIconName('checkmark-outline');
    } else {
      setIconName('close-outline');
    }
  };

  return (
    <View
      style={[
        styles.container,
        {backgroundColor: colors.primaryContainer, borderRadius: roundness},
        style,
      ]}>
      <View style={styles.titleContainer}>
        <Text style={[styles.title, {color: colors.onPrimaryContainer}]}>
          Задание №{number}: {title}
        </Text>
        <Icon
          size={24}
          name={iconName}
          color={iconName === 'checkmark-outline' ? 'green' : 'red'}
        />
      </View>
      <Text style={[styles.description, {color: colors.onPrimaryContainer}]}>
        {description}
      </Text>
      <View style={styles.userActionsBlock}>
        <BaseInput
          inputProps={{
            editable: !isBlocked,
            selectTextOnFocus: !isBlocked,
            onChange: handleInputChange,
            placeholder: 'Введите свой ответ',
            placeholderTextColor: colors.onSurface,
          }}
          style={{
            ...styles.inputContainer,
            backgroundColor: colors.surface,
            color: colors.onSurface,
          }}
          inputStyle={{
            ...styles.input,
            backgroundColor: colors.surface,
            color: colors.onSurface,
          }}
        />
        <Button
          style={[styles.button, {borderRadius: roundness}]}
          buttonColor={colors.primary}
          textColor={colors.onPrimary}
          onPress={handleButtonPress}
          disabled={isBlocked}>
          Завершить
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    padding: 15,
  },
  titleContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    gap: 20,
  },
  title: {
    fontFamily: 'Inter Regular',
    fontSize: 16,
    fontWeight: '500',
  },
  description: {
    fontFamily: 'Inter Regular',
    marginTop: 8,
    fontSize: 12,
    fontWeight: '400',
  },
  userActionsBlock: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 8,
  },
  button: {
    width: '20%',
    marginLeft: 10,
  },
  inputContainer: {
    flexGrow: 1,
    borderRadius: 3,
    height: 40,
  },
  input: {
    padding: 13,
    borderRadius: 3,
    fontFamily: 'Inter Regular',
    fontSize: 12,
  },
});

export default TaskWithInput;
