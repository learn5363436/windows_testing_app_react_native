import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Text, StyleProp, ViewStyle} from 'react-native';
import {useTheme, List, Button} from 'react-native-paper';
// @ts-ignore
import Icon from 'react-native-vector-icons/dist/Ionicons';

type answer = {id: string; title: string; isTrue: boolean};

interface TaskWithSelectProps {
  number: number;
  title: string;
  description: string;
  variants: answer[];
  style?: StyleProp<ViewStyle>;
}

const TaskWithSelect: React.FC<TaskWithSelectProps> = ({
  number,
  title,
  description,
  variants,
  style,
}) => {
  const {colors, roundness} = useTheme();

  const [isBlocked, setIsBlocked] = useState<boolean>(false);
  const [userAnswers, setUserAnswers] = useState<answer[]>([]);

  const [iconName, setIconName] = useState<string>('');
  const [listExpanded, setListExpanded] = useState<boolean>(false);
  const [listTitle, setListTitle] = useState<string>('Варианты');

  const multiple = variants.filter(value => value.isTrue).length > 1;

  useEffect(() => {
    if (userAnswers.length === 0) {
      setListTitle('Варианты');
    } else {
      const titleList = userAnswers.map(answer => answer.title);
      setListTitle(titleList.join(', '));
    }
  }, [userAnswers]);

  const selectAnswer = (variant: answer): void => {
    if (!multiple) {
      setUserAnswers([variant]);
    } else {
      if (!userAnswers.includes(variant)) {
        setUserAnswers(prevState => [...prevState, variant]);
      } else {
        setUserAnswers(userAnswers.filter(answer => answer !== variant));
      }
    }
  };

  const handleListExpand = (): void => setListExpanded(!listExpanded);

  const allAnswersCorrect = (): boolean =>
    variants.every(variant => {
      // Проверяем, что для каждого правильного ответа он был выбран пользователем
      return variant.isTrue ? userAnswers.includes(variant) : true;
    });

  const checkUserAnswers = (): void => {
    if (allAnswersCorrect()) {
      setIconName('checkmark-outline');
    } else {
      setIconName('close-outline');
    }
  };

  const complete = (): void => {
    setIsBlocked(true);
    setListExpanded(false);
    checkUserAnswers();
  };

  const selectedItemIcon = (): React.JSX.Element => {
    return (
      <Icon size={12} name="checkmark-done-outline" color={colors.onPrimary} />
    );
  };

  const variantsListItems: React.JSX.Element[] = variants.map(variant => (
    <List.Item
      key={variant.id}
      title={variant.title}
      style={[
        styles.listItem,
        {backgroundColor: colors.primary, borderColor: colors.inversePrimary},
      ]}
      titleStyle={{color: colors.onPrimary, fontSize: 12}}
      onPress={() => {
        !isBlocked ? selectAnswer(variant) : null;
      }}
      right={
        userAnswers.map(answer => answer.id).includes(variant.id)
          ? selectedItemIcon
          : undefined
      }
    />
  ));

  return (
    <View
      style={[
        styles.container,
        {backgroundColor: colors.primaryContainer, borderRadius: roundness},
        style,
      ]}>
      <View style={styles.titleContainer}>
        <Text style={[styles.title, {color: colors.onPrimaryContainer}]}>
          Задание №{number}: {title}
        </Text>
        <Icon
          size={24}
          name={iconName}
          color={iconName === 'checkmark-outline' ? 'green' : 'red'}
        />
      </View>
      <Text style={[styles.description, {color: colors.onPrimaryContainer}]}>
        {description}
      </Text>
      <View style={styles.userActionsBlock}>
        <List.Section
          style={[
            styles.listSection,
            {backgroundColor: colors.surface, borderRadius: roundness},
          ]}>
          <List.Accordion
            title={listTitle}
            style={[
              styles.listAccordion,
              {backgroundColor: colors.surface, borderRadius: roundness},
            ]}
            titleStyle={{color: colors.onSurface, fontSize: 12}}
            expanded={listExpanded}
            onPress={() => {
              !isBlocked ? handleListExpand() : null;
            }}>
            {variantsListItems}
          </List.Accordion>
        </List.Section>
        <Button
          style={[styles.button, {borderRadius: roundness}]}
          contentStyle={{height: '100%'}}
          buttonColor={colors.primary}
          textColor={colors.onPrimary}
          onPress={complete}>
          Завершить
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    padding: 15,
  },
  titleContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    gap: 20,
  },
  title: {
    fontFamily: 'Inter',
    fontSize: 16,
    fontWeight: '500',
  },
  description: {
    fontFamily: 'Inter',
    marginTop: 8,
    fontSize: 12,
    fontWeight: '400',
  },
  userActionsBlock: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 8,
    alignItems: 'flex-start',
  },
  button: {
    width: '20%',
    marginLeft: 10,
    height: 46,
  },
  listSection: {
    flexGrow: 1,
    marginTop: 0,
    marginBottom: 0,
  },
  listAccordion: {
    margin: 0,
    paddingTop: 0,
    paddingBottom: 0,
  },
  listItem: {
    borderBottomWidth: 1,
  },
});

export default TaskWithSelect;
