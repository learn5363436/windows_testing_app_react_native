import React from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  StyleProp,
  ViewStyle,
  TextStyle,
  TextInputProps,
} from 'react-native';

interface BaseInputProps {
  inputProps?: TextInputProps;
  style?: StyleProp<ViewStyle | TextStyle>;
  inputStyle?: StyleProp<ViewStyle | TextStyle>;
}

const BaseInput: React.FC<BaseInputProps> = ({
  inputProps,
  style,
  inputStyle,
}) => {
  return (
    <View style={[styles.container, style]}>
      <TextInput
        underlineColorAndroid={'transparent'}
        style={[styles.input, inputStyle]}
        {...inputProps}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderBottomColor: 'transparent',
    borderBottomWidth: 0,
    overflow: 'hidden',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    width: '101%',
    height: '110%',
    padding: 10,
  },
});

export default BaseInput;
