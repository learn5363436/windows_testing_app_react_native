import React from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  GestureResponderEvent,
} from 'react-native';
import customFontsLinks from '../../utils/customFontsLinks';

interface BaseButtonProps {
  onPress?: (e: GestureResponderEvent) => void | undefined;
  title: string;
  style?: object;
  disabled?: boolean;
}

const BaseButton: React.FC<BaseButtonProps> = ({
  onPress,
  title,
  style,
  disabled,
}) => {
  return (
    <TouchableOpacity
      style={[styles.BaseButton, style]}
      onPress={onPress}
      disabled={disabled}>
      <Text style={[styles.BaseButtonTitle]}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  BaseButton: {
    width: '100%',
    height: 40,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  BaseButtonTitle: {
    fontFamily: customFontsLinks.interLight,
    fontWeight: '300',
    fontSize: 16,
  },
});

export default BaseButton;
