import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {useTheme, Button} from 'react-native-paper';

type answer = {id: string; isTrue: boolean};

interface InteractiveTaskProps {
  setAnswer: React.Dispatch<React.SetStateAction<answer | undefined>>;
  isBlocked: boolean;
}

const SelectHeaderTask: React.FC<InteractiveTaskProps> = ({
  setAnswer,
  isBlocked,
}) => {
  const {colors, roundness} = useTheme();

  return (
    <View
      style={[
        styles.container,
        {backgroundColor: colors.surface, borderRadius: roundness},
      ]}>
      <TouchableOpacity
        style={[styles.header, {backgroundColor: colors.primary}]}
        onPress={() => setAnswer({id: '1', isTrue: true})}
        disabled={isBlocked}>
        <View style={styles.headerNav}>
          <Text style={styles.headerNavText}>Home</Text>
          <Text style={styles.headerNavText}>About</Text>
          <Text style={styles.headerNavText}>Contact</Text>
        </View>
        <Button
          buttonColor={colors.onPrimary}
          style={[styles.headerButton, {borderRadius: roundness}]}>
          Login
        </Button>
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.body, {backgroundColor: colors.primary}]}
        onPress={() => setAnswer({id: '2', isTrue: false})}
        disabled={isBlocked}>
        <Text>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus
          alias aliquam architecto aspernatur beatae dignissimos dolore eaque
          earum eius eum facilis inventore ipsa labore laborum natus odio
          perferendis placeat porro quia quo ratione repellendus sapiente sint,
          sunt suscipit totam ullam velit vitae voluptates! A animi distinctio,
          dolorem doloribus eos eum exercitationem facilis in incidunt ipsa
          itaque labore laboriosam magnam maxime molestiae nesciunt nisi nostrum
          odio provident, repudiandae sed vitae voluptatum! Atque dolorum eos
          error et eveniet ex, maxime numquam perferendis praesentium quasi sed
          vitae! Accusantium ad aspernatur consectetur cumque dolores dolorum
          enim eos error expedita fugiat ipsa laudantium magnam magni, minus
          nulla obcaecati odio officia omnis porro praesentium provident quam
          quas quisquam rerum sed similique soluta tempore tenetur totam ut vel
          veritatis voluptas voluptatibus. A accusamus ad aliquam autem
          blanditiis consequuntur, corporis cupiditate earum eos facilis harum
          in itaque magnam magni molestiae nulla odit officia quas, quidem
          quisquam sapiente, suscipit ut voluptatibus? Asperiores beatae
          consequuntur corporis debitis deserunt dignissimos dolore, eaque earum
          labore libero mollitia non officia perferendis quaerat, quas quibusdam
          quod reprehenderit rerum saepe sit, suscipit temporibus voluptas. A
          deserunt dolores dolorum eligendi error esse eveniet facilis fuga, id
          illo illum magnam minima molestiae mollitia nam numquam officia
          officiis provident quae rerum sint tenetur ullam velit voluptas
          voluptates! At commodi consectetur culpa debitis delectus deserunt
          distinctio, eaque expedita iste magni nemo nihil, numquam perspiciatis
          quaerat quidem ullam unde! Amet, deserunt, est. Accusamus aperiam
          cumque, dolores explicabo in inventore non numquam obcaecati odit
          optio quibusdam quod totam, ut. Accusamus enim ex, explicabo id quam
          reprehenderit. Deserunt dolor dolorum eos error et, illum inventore
          laborum natus nostrum officia possimus quis quisquam similique sit
          tempore temporibus totam veritatis voluptas voluptate voluptatem. Amet
          atque aut cum dolorem doloremque, dolorum explicabo ipsa labore minus
          nisi obcaecati perspiciatis quod ratione? Ad aperiam consequatur
          cupiditate dolorum earum harum inventore maiores minus, optio
          perferendis praesentium quaerat quas quasi reiciendis saepe tempore
          voluptatum! Corporis cum cumque ex exercitationem? Accusantium, at, ex
          exercitationem inventore non nulla porro possimus reiciendis
          repudiandae sapiente sint tenetur vel voluptatum. Ab aperiam
          architecto asperiores blanditiis corporis culpa cumque cupiditate,
          distinctio doloremque dolorum est ex exercitationem magni maxime
          necessitatibus nesciunt nisi nulla odit placeat possimus quam quis
          ratione rerum sed similique sunt suscipit tempore! Adipisci amet
          aperiam at aut blanditiis consectetur consequatur corporis, cum
          deleniti dolor dolorum ea eaque eos eum exercitationem hic impedit
          iure magni modi, molestias numquam perferendis quaerat reiciendis,
          repellendus rerum similique sit veritatis? Aspernatur doloremque ipsam
          nesciunt reiciendis totam? Aperiam deserunt eius eveniet harum illo
          incidunt ipsam, labore odit soluta. Amet, delectus dolor eligendi esse
          excepturi illo modi natus officiis possimus quaerat qui quia
          reiciendis, repellat repudiandae rerum tempora, tenetur voluptatem. Ab
          accusantium architecto aspernatur at cumque, delectus distinctio dolor
          dolore dolores earum eius eligendi eos excepturi harum illum impedit
          inventore iste itaque laudantium mollitia nostrum numquam optio qui
          quos repudiandae soluta ut vitae! Delectus hic magnam nesciunt
          perferendis, tenetur unde. Alias aliquam animi aperiam aut cupiditate
          delectus exercitationem fuga id ipsam optio quam quis rem soluta
          tenetur veniam, voluptatem.
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.footer, {backgroundColor: colors.primary}]}
        onPress={() => setAnswer({id: '3', isTrue: false})}
        disabled={isBlocked}>
        <Text style={styles.footerText}>Copyright 2005-2024 SiteCare, LLC</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '80%',
    padding: 6,
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderWidth: 1,
    borderRightWidth: 0,
    borderLeftWidth: 0,
  },
  headerNav: {
    display: 'flex',
    flexDirection: 'row',
    gap: 6,
  },
  headerNavText: {
    fontWeight: 'bold',
  },
  headerButton: {
    width: 80,
  },
  body: {
    height: 256,
    padding: 10,
  },
  footer: {
    display: 'flex',
    flexDirection: 'row',
    borderTopWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20,
    paddingBottom: 20,
  },
  footerText: {
    flexWrap: 'wrap',
    flexShrink: 1,
  },
});

export default SelectHeaderTask;
