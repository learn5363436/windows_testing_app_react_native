import React, {useState} from 'react';
import {
  FlatList,
  StyleProp,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import DragList, {DragListRenderItemInfo} from 'react-native-draglist';
// @ts-ignore
import Icon from 'react-native-vector-icons/dist/Ionicons.js';
import {Button, useTheme} from 'react-native-paper';

type answer = {id: string; title: string; truePosition: number};

interface TaskWithSelectProps {
  number: number;
  title: string;
  description: string;
  variants: answer[];
  style?: StyleProp<ViewStyle>;
}

const TaskWithOrder: React.FC<TaskWithSelectProps> = ({
  number,
  title,
  description,
  variants,
  style,
}) => {
  const {colors, roundness} = useTheme();
  const [isBlocked, setIsBlocked] = useState<boolean>(false);

  const [iconName, setIconName] = useState<string>('');
  const [userAnswers, setUserAnswers] = useState<answer[]>([...variants]);

  const keyExtractor = (userAnswer: answer) => {
    return userAnswer.id.toString();
  };

  const onReordered = async (fromIndex: number, toIndex: number) => {
    const copy = [...userAnswers];
    const removed = copy.splice(fromIndex, 1);

    copy.splice(toIndex, 0, removed[0]);
    setUserAnswers(copy);
  };

  const allAnswersCorrect = () =>
    userAnswers.every((variant, index) => variant.truePosition === index + 1);

  const checkAnswer = () => {
    if (allAnswersCorrect()) {
      setIconName('checkmark-outline');
    } else {
      setIconName('close-outline');
    }
  };

  const complete = (): void => {
    setIsBlocked(true);
    checkAnswer();
  };

  const renderListItem = (info: DragListRenderItemInfo<string>) => {
    const {item, onDragStart, onDragEnd, isActive} = info;

    return (
      <TouchableOpacity
        style={[
          styles.listItem,
          {
            backgroundColor: colors.primary,
            borderRadius: roundness,
            borderColor: colors.inversePrimary,
            borderWidth: isActive ? 2 : 0,
          },
        ]}
        key={item?.id?.toString()}
        onPressIn={!isBlocked ? onDragStart : undefined}
        onPressOut={onDragEnd}>
        <Text style={[styles.listItemText, {color: colors.onPrimary}]}>
          {item?.title}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <View
      style={[
        styles.container,
        {backgroundColor: colors.primaryContainer, borderRadius: roundness},
        style,
      ]}>
      <View style={styles.titleContainer}>
        <Text style={[styles.title, {color: colors.onPrimaryContainer}]}>
          Задание №{number}: {title}
        </Text>
        <Icon
          size={24}
          name={iconName}
          color={iconName === 'checkmark-outline' ? 'green' : 'red'}
        />
      </View>
      <Text style={[styles.description, {color: colors.onPrimaryContainer}]}>
        {description}
      </Text>
      <View style={styles.userActionsBlock}>
        <View style={styles.listSection}>
          <FlatList
            data={[]}
            keyExtractor={(e, i) => 'dom' + i.toString()}
            scrollEnabled={false}
            ListEmptyComponent={null}
            renderItem={null}
            ListHeaderComponent={
              <DragList
                data={userAnswers}
                keyExtractor={keyExtractor}
                onReordered={onReordered}
                renderItem={renderListItem}
              />
            }
          />
        </View>
        <Button
          style={[styles.button, {borderRadius: roundness}]}
          contentStyle={{height: '100%'}}
          buttonColor={colors.primary}
          textColor={colors.onPrimary}
          onPress={complete}>
          Завершить
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    padding: 15,
  },
  titleContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    gap: 20,
  },
  title: {
    fontFamily: 'Inter',
    fontSize: 16,
    fontWeight: '500',
  },
  description: {
    fontFamily: 'Inter',
    marginTop: 8,
    fontSize: 12,
    fontWeight: '400',
  },
  userActionsBlock: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 8,
    alignItems: 'flex-start',
  },
  button: {
    width: '20%',
    marginLeft: 10,
    height: 46,
  },
  listSection: {
    flexGrow: 1,
    marginTop: 0,
    marginBottom: 0,
  },
  listItem: {
    padding: 15,
    marginBottom: 3,
  },
  listItemText: {
    fontSize: 12,
  },
});

export default TaskWithOrder;
