/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {PaperProvider} from 'react-native-paper';

import theme from './theme.ts';
import Router from './router';

function App(): React.JSX.Element {
  return (
    <PaperProvider theme={theme}>
      <Router />
    </PaperProvider>
  );
}

export default App;
