import React, {useState} from 'react';
import {
  SafeAreaView,
  useColorScheme,
  StatusBar,
  StyleSheet,
  ViewStyle,
  ScrollView,
  View,
} from 'react-native';
import type {PropsWithChildren} from 'react';
import TaskWithInput from '../components/TaskWithInput';
import TaskWithSelect from '../components/TaskWithSelect.tsx';
import {useTheme} from 'react-native-paper';

import TaskWithOrder from '../components/TaskWithOrder.tsx';
import InteractiveTask, {
  interactiveTaskAnswer,
} from '../components/InteractiveTask.tsx';

type HomeScreenProps = PropsWithChildren<{
  navigation: any;
}>;

const HomeScreen = ({navigation}: HomeScreenProps) => {
  const isDarkMode = useColorScheme() === 'dark';
  const {colors} = useTheme();

  return (
    <SafeAreaView style={styles.mainScreenSafeArea}>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={colors.background}
      />
      <ScrollView
        style={[
          styles.mainScreenContainer,
          {backgroundColor: colors.background},
        ]}>
        <TaskWithInput
          number={1}
          title="Тег с которого начинается любой HTML документ"
          description="Введите тег с которого начинается любой HTML документ."
          solution="<!DOCTYPE HTML>"
          style={{margin: 10}}
        />
        <TaskWithSelect
          number={2}
          title="Какое css свойство отвечает за выравнивания текста"
          description="Выбирете правильный ответ из списка."
          variants={[
            {id: '1', title: 'align-text', isTrue: true},
            {id: '2', title: 'overflow', isTrue: false},
            {id: '3', title: 'text-decoration-line', isTrue: false},
          ]}
          style={{margin: 10}}
        />
        <TaskWithSelect
          number={3}
          title="Какой тег используется для создания списка"
          description="Выбирете правильные ответы из списка."
          variants={[
            {id: '1', title: '<li>', isTrue: false},
            {id: '2', title: '<br>', isTrue: false},
            {id: '3', title: '<ul>', isTrue: true},
            {id: '4', title: '<ol>', isTrue: true},
          ]}
          style={{margin: 10}}
        />
        <View>
          <TaskWithOrder
            number={4}
            title="В каком порядке идут теги на странице"
            description="Расположите в правильном порядке элементы."
            variants={[
              {id: '1', title: '<body>', truePosition: 2},
              {id: '2', title: '<head>', truePosition: 1},
              {id: '3', title: '<header>', truePosition: 3},
              {id: '4', title: '<footer>', truePosition: 4},
            ]}
            style={{margin: 10}}
          />
        </View>
        <InteractiveTask
          number={5}
          title="Выбирите элемент header"
          description="Найдите и выбирите необходимый элемент на странице."
        />
      </ScrollView>
    </SafeAreaView>
  );
};

type HomeStyle = {
  mainScreenSafeArea: ViewStyle;
  mainScreenContainer: ViewStyle;
};

const styles = StyleSheet.create<HomeStyle>({
  mainScreenSafeArea: {
    flex: 1,
  },
  mainScreenContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
});

export default HomeScreen;
